package com.hotel.poc.web;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hotel.poc.cb.domain.Room;
import com.hotel.poc.cb.service.RoomService;

@Controller
public class RoomController {

	@Autowired
	private RoomService roomService;
	
	@Inject
	@Named("webSocketFlow.input")
	private MessageChannel webSocketFlow;
	

	@RequestMapping("/room")
	@ResponseBody
	public ModelAndView index() {
		return new ModelAndView("room/index", "rooms",
				roomService.findAllRooms());
	}

	@RequestMapping("/room/edit/{id}")
	@ResponseBody
	public ModelAndView edit(@PathVariable String id) {
		Room room = roomService.findById(id);
		room.setOccupied(!room.getOccupied());
		roomService.saveRoom(room);
		return new ModelAndView("room/index", "rooms",
				roomService.findAllRooms());
	}

}
