package com.hotel.poc.aspect;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.stereotype.Component;

import com.hotel.poc.cb.domain.Room;
import com.hotel.poc.eventbus.Publisher;

@Aspect
@Component
class RoomServiceTriggerAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(RoomServiceTriggerAspect.class);
	private final CounterService counterService;

	// @Inject
	// @Named("webSocketFlow.input")
	// private MessageChannel webSocketFlow;

	@Autowired
	Publisher publisher;

	@Autowired
	public RoomServiceTriggerAspect(CounterService counterService) {
		this.counterService = counterService;
	}

	@AfterReturning(pointcut = "execution(* com.hotel.poc.cb.service.RoomService.saveRoom(com.hotel.poc.cb.domain.Room)) && args(room)", argNames = "room")
	public void afterCallingShowChoosenIdType(Room room) {
		LOGGER.info("Triggered after calling saveRoom()");
		counterService.increment("counter.calls.room");
		counterService.increment("counter.calls.room." + room.getNumber());
		publisher.publish(room.getNumber());
		// webSocketFlow.send(MessageBuilder.withPayload(room.getNumber()).build());
	}

}
