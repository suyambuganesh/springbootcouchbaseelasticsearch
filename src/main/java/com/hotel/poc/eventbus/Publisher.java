package com.hotel.poc.eventbus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import reactor.bus.Event;
import reactor.bus.EventBus;

@Service
public class Publisher {

	@Autowired
	EventBus eventBus;

	public void publish(String roomNumber) {
		eventBus.notify("room", Event.wrap(roomNumber));
	}

}
