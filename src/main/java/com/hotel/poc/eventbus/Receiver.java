package com.hotel.poc.eventbus;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import reactor.bus.Event;
import reactor.fn.Consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hotel.poc.es.service.InventoryService;

@Service
public class Receiver implements Consumer<Event<String>> {

	private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);

	@Autowired
	InventoryService inventoryService;

	@Inject
	@Named("webSocketFlow.input")
	private MessageChannel webSocketFlow;

	public void accept(Event<String> ev) {
		LOGGER.info("Received Event Data: {}", ev.getData());
		try {
			// the data replication from Couchbase to Elasticsearch takes
			// atleast 800ms... which is pretty good in a dev laptop I guess...
			Thread.sleep(800);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
		}
		Map<String, Integer> hotelOccupancyStatus = inventoryService.getHotelOccupancyStatus();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			webSocketFlow.send(MessageBuilder.withPayload(objectMapper.writeValueAsString(hotelOccupancyStatus)).build());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}