package com.hotel.poc;

import static reactor.bus.selector.Selectors.$;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import reactor.Environment;
import reactor.bus.EventBus;
import reactor.spring.context.config.EnableReactor;

import com.hotel.poc.eventbus.Publisher;
import com.hotel.poc.eventbus.Receiver;

@Configuration
@EnableReactor
public class ReactorEventBusConfig {

	static {
		Environment.initializeIfEmpty().assignErrorJournal();
	}

	@Bean
	EventBus eventBus() {
		return EventBus.create(Environment.get());
	}

	@Autowired
	private Receiver receiver;

	@Autowired
	private Publisher publisher;

	@PostConstruct
	public void onStartUp() {
		eventBus().on($("room"), receiver);
	}

}
