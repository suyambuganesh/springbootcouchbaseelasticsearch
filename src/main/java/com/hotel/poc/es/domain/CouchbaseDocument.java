package com.hotel.poc.es.domain;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "hotel", type = "couchbaseDocument", shards = 1, replicas = 0, refreshInterval = "-1", indexStoreType = "memory")
public class CouchbaseDocument {

	private String id;
	private int score;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

}
