package com.hotel.poc.es.service;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.hotel.poc.es.domain.CouchbaseDocument;

public interface CouchbaseDocumentRepository extends
		ElasticsearchRepository<CouchbaseDocument, String> {

}
