package com.hotel.poc.es.service;

import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.FacetedPage;
import org.springframework.data.elasticsearch.core.facet.request.TermFacetRequestBuilder;
import org.springframework.data.elasticsearch.core.facet.result.Term;
import org.springframework.data.elasticsearch.core.facet.result.TermResult;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import com.hotel.poc.es.domain.CouchbaseDocument;

@Service("inventoryService")
public class InventoryServiceImpl implements InventoryService {

	@Autowired
	CouchbaseDocumentRepository couchbaseDocumentRepository;

	@Autowired
	ElasticsearchTemplate elasticsearchTemplate;

	@Override
	public Map<String, Integer> getHotelOccupancyStatus() {
		Map<String, Integer> statusMap = new HashMap<>();
		SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(matchAllQuery()).withFacet(new TermFacetRequestBuilder("status").fields("occupied").build()).build();
		FacetedPage<CouchbaseDocument> result = elasticsearchTemplate.queryForPage(searchQuery, CouchbaseDocument.class);
		int total = result.getNumberOfElements();
		TermResult termResult = (TermResult) result.getFacet("status");
		for (Term term : termResult.getTerms()) {
			statusMap.putIfAbsent(term.getTerm().equalsIgnoreCase("T") ? "OCCUPIED" : "AVAILABLE",(term.getCount()*100)/total);
		}
		return statusMap;
	}

}
