package com.hotel.poc.es.service;

import java.util.Map;

public interface InventoryService {

	public Map<String, Integer> getHotelOccupancyStatus();

}
