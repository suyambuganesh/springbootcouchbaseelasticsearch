package com.hotel.poc.cb.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;

@Document
public class Room {

	public Room(String number, String description, Boolean occupied) {
		super();
		this.number = number;
		this.description = description;
		this.occupied = occupied;
	}

	@Id
	private String number;
	@Field
	private String description;
	@Field
	private Boolean occupied;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getOccupied() {
		return occupied;
	}

	public void setOccupied(Boolean occupied) {
		this.occupied = occupied;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result
				+ ((occupied == null) ? 0 : occupied.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Room other = (Room) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (occupied == null) {
			if (other.occupied != null)
				return false;
		} else if (!occupied.equals(other.occupied))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("Room [number=%s, description=%s, occupied=%s]",
				number, description, occupied);
	}

	public String getEditLink() {
		return "/room/edit/" + getNumber();
	}

}
