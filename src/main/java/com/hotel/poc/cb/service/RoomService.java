package com.hotel.poc.cb.service;

import java.util.List;

import com.hotel.poc.cb.domain.Room;

public interface RoomService {

	List<Room> findAllRooms();

	Room saveRoom(Room room);
	
	Room findById(String id);

}
