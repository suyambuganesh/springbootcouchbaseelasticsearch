package com.hotel.poc.cb.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.hotel.poc.cb.domain.Room;

@Service("roomService")
public class RoomServiceImpl implements RoomService {

	@Autowired
	RoomRepository roomRepository;

	@Override
	public List<Room> findAllRooms() {
		return Lists.newArrayList(roomRepository.findAll());
	}

	@Override
	public Room saveRoom(Room room) {
		return roomRepository.save(room);
	}

	@PostConstruct
	public void insertData() {
		Room room = new Room("420", "fully furnished room", Boolean.FALSE);
		roomRepository.save(room);
		room = new Room("421", "semi furnished room", Boolean.TRUE);
		roomRepository.save(room);
		room = new Room("422", "fully furnished room", Boolean.TRUE);
		roomRepository.save(room);
		room = new Room("423", "semi furnished room", Boolean.FALSE);
		roomRepository.save(room);
	}

	@Override
	public Room findById(String id) {
		return roomRepository.findOne(id);
	}

}
