package com.hotel.poc.cb.service;

import org.springframework.data.repository.CrudRepository;

import com.hotel.poc.cb.domain.Room;

public interface RoomRepository extends CrudRepository<Room, String> {

}
