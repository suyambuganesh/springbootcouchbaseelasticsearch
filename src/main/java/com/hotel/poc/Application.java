package com.hotel.poc;

import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.reactor.ReactorAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.support.Function;
import org.springframework.integration.websocket.ServerWebSocketContainer;
import org.springframework.integration.websocket.outbound.WebSocketOutboundMessageHandler;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.support.MessageBuilder;

@SpringBootApplication(exclude = {ReactorAutoConfiguration.class})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	ServerWebSocketContainer serverWebSocketContainer() {
		return new ServerWebSocketContainer("/messages").withSockJs();
	}

	@Bean
	MessageHandler webSocketOutboundAdapter() {
		return new WebSocketOutboundMessageHandler(serverWebSocketContainer());
	}

	@Bean(name = "webSocketFlow.input")
	MessageChannel requestChannel() {
		return new DirectChannel();
	}

	@Bean
	IntegrationFlow webSocketFlow() {
		return f -> {
			Function<Message, Object> splitter = m -> serverWebSocketContainer()
					.getSessions()
					.keySet()
					.stream()
					.map(s -> MessageBuilder
							.fromMessage(m)
							.setHeader(
									SimpMessageHeaderAccessor.SESSION_ID_HEADER,
									s).build()).collect(Collectors.toList());
			f.split(Message.class, splitter)
					.channel(c -> c.executor(Executors.newCachedThreadPool()))
					.handle(webSocketOutboundAdapter());
		};
	}

}
