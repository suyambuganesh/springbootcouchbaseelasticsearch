# README #

Spring Boot based sample application to demonstrate the integration of Couchbase and Elasticsearch

Couchbase provides a elasticsearch transport plugin using XDCR. This is based on this plugin.
Attached the complete architecture of the POC, All the components are integrated into single spring boot application. 

The source code is available in https://bitbucket.org/suyambuganesh/springbootcouchbaseelasticsearch

Disclaimer:
-	This is very bare minimal working POC with all the components integrated together. And it can be used as test bed for various things.

This is the flow of data,

1.	The front desk user sees the list of rooms which are retrieved from couchbase
2.	Updates the room’s occupancy status
3.	The change is updated in the service layer
4.	The updated document is persisted in Couchbase using spring data couchbase
5.	I have written an aspect which intercepts the modifications to room document 
a.	In the meantime, couchbase replicates the data modifications to elasticsearch through couchbase elasticsearch transport plugin, which was pretty good in my laptop
6.	The aspect then creates an event and publishes through publisher
7.	The publisher puts the event to the event bus (Reactor event bus, based on LMAX disruptor)
8.	The event bus notifies the event receiver, the receiver waits for 800ms for couchbase to replicate the data to elasticsearch
9.	The event receiver fetches the status of all the rooms using spring data elastic search. The query to ES is faceted search.
10.	The result is transformed to percentage and converted to json string  and pushes the json string to Spring integration websocket channel
11.	The data is send to the dashboard UI, which is created using Highcharts 
12.	So the dashboard for manager is updated in realtime almost within 1.4 seconds, which I think it’s amazing

I have used the following components

-	Java 8
-	Tomcat 8
-	Spring Boot
-	Spring MVC
-	Spring Thymeleaf templates
-	Spring Data couchbase
-	Spring Data elasticsearch
-	Spring integration websocket
-	Spring integration DSL
-	Reactor Event Bus (http://projectreactor.io/ - based on LMAX disruptor – A must read article by Martin fowler http://martinfowler.com/articles/lmax.html
-	Spring AOP
-	Couchbase 3
-	Elasticsearch 1.4.4
-	Couchbase – ElasticSearch transport plugin version 2

I didn’t do any benchmarks, but you can use Yahoo’s YCSB for it..

Using curl in windows is not that easy when compared to Mac and I just use browser based rest client for all curl usages. And head plugin for elasticsearch queries

Set up:

-	Install couchbase (Docker for windows is not working….so complete installation required)
-	Install elasticsearch (Docker container is available, which works on windows with boot2docker)
-	Install couchbase elasticsearch transport plugin and follow the setup in the doc (http://docs.couchbase.com/admin/elastic/intro-elasticsearch.html)
-	And you have to create a View in couchbase for the document Room
-	The app is sprint boot base and you can open the app in sts and run as springboot
-	Open localhost:8080/room in browser 1
-	Open localhost:8080/status.html in browser 2
-	Update anything in browser 1 should reflect immediately in browser 2